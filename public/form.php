<!DOCTYPE html>
<html lang="ru">
<head>

  <meta charset="UTF-8">
  <title>Bc5</title>
  <link rel="stylesheet" type="text/css" href="style.css">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <link rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

</head>
<body>
  <form method="POST" action="index.php">
    <h1>Форма.</h1>
     <?php 
    if (!empty($messages)) {
      print('<div id="messages">');
      // Выводим все сообщения.
      foreach ($messages as $message) {
        print($message);
      }
      print('</div>');
    }
    ?>
    <div>
      <div>
        <label <?php 
          if ($errors['name']) {print 'class="error-label"';} 
          ?> >
          Имя:<br>
          <input type="text" name="name" value="<?php print $errors['name'] ? $errors_value['name'] : $values['name'];?>">
        </label><br>
        <label <?php 
          if ($errors['yob']) {print 'class="error-label"';} 
          ?> >
        Год рождения:<br>
        <select name="yob">
          <?php
          for ($i=1980; $i <= 2000; $i++) { 
            echo "<option"; if($i==$values['yob']) {echo " selected";} echo ">" . $i . "</option>";
          }
          ?>
        </select>
        </label><br>
          Количество конечностей: <br>
        <label <?php 
          if ($errors['limbs']) {print 'class="error-label"';} 
          ?>>
          <input type="radio" name="limbs" value="5" <?php if($values['limbs'] == "5") {print "checked";} ?>> 5
        </label>
        <label <?php 
          if ($errors['limbs']) {print 'class="error-label"';} 
          ?>>
          <input type="radio" name="limbs" value="6" <?php if($values['limbs'] == "6") {print "checked";} ?>> больше 5
        </label>
        <label <?php 
          if ($errors['limbs']) {print 'class="error-label"';} 
          ?>>
          <input type="radio" name="limbs" value="4" <?php if($values['limbs'] == "4") {print "checked";} ?>> меньше 5
        </label> <br>
      </div>
      <div>
      <label <?php 
          if ($errors['email']) {print 'class="error-label"';} 
          ?>>
        Email:<br>
        <input id="email" type="email" name="email" value="<?php
         print $errors['email'] ? $errors_value['email'] : $values['email']; 
         ?>">
      </label><br>
      Пол:<br>
      <label <?php 
          if ($errors['sex']) {print 'class="error-label"';}
          ?>>
        <input type="radio" name="sex" value="1" <?php if($values['sex'] == "1") {print "checked";} ?>> М
      </label>
      <label <?php 
          if ($errors['sex']) {print 'class="error-label"';}
          ?>>
        <input type="radio" name="sex" value="2" <?php if($values['sex'] == "2") {print "checked";} ?>> Ж
      </label> <br>
      <label <?php 
          if ($errors['sp']) {print 'class="error-label"';}
          ?>>
        Суперспособность: <br>
        <select name="superpowers[]" multiple="multiple">
          <option <?php for($i = 0; $i<3; $i++){ if(strcmp($values["sp$i"], "Бессмертие")==0) {print "selected";}} ?>> Бессмертие </option>
          <option <?php for($i = 0; $i<3; $i++){ if(strcmp($values["sp$i"], "Прохождение сквозь стены")==0) {print "selected";}} ?>> Прохождение сквозь стены </option>
          <option <?php for($i = 0; $i<3; $i++){ if(strcmp($values["sp$i"], "Левитация")==0) {print "selected";}} ?>> Левитация </option>
        </select>
      </label> <br>
      </div>
    </div>
    <div>
      <div>
        Биография: <br>
        <label>
          <textarea name="biography" rows="5" cols="50"><?php !empty($values['biography']) ? print $values['biography'] : print ""; ?></textarea>
        </label> <br>
      </div>
      <div>
        <label <?php 
          if ($errors['checkbox']) {print 'class="error-label"';} 
          ?>>
          <input type="checkbox" name="checkbox" value="1" <?php if($values['checkbox'] == "1") {print "checked";} ?>>
          С контрактом ознакомлен
        </label>
        <input type="submit" name="Отправить">
      </div>
    </div>
  </form>

  <footer>
      <h2>(c) Дмитрий Щербин</h2>
  </footer>

</body>
</html>
